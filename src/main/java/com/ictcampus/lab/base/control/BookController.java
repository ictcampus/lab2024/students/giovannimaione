package com.ictcampus.lab.base.control;
import com.ictcampus.lab.base.control.exception.NotFoundException;
import com.ictcampus.lab.base.control.model.BookRequest;
import com.ictcampus.lab.base.control.model.BookResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
@Slf4j
public class BookController {
	private List<BookResponse> List = new ArrayList<>();

	public BookController() {
		this.List = GenerateBooks();
	}


	@GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
	public List<BookResponse> getBooks() {
		log.info("Restituisco la lista dei libri {{}}", List);

		return this.List;
	}


	private List<BookResponse> GenerateBooks() {

		log.info("Generazione della lista di libri");

		Long index = 1L;
		List<BookResponse> listTmp = new ArrayList<>(Arrays.asList(

				new BookResponse(index, " Divina commedia", "Dante","02462506" ),
				new BookResponse(++index, " Le storie dell'arte", "Hoepli2","65518959" ),
				new BookResponse(++index, " Il marketing della moda e dei prodotti lifestyle", "Carocci","2558635" )
		));

		Collections.shuffle(listTmp);
		return listTmp;
	}

	@GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public BookResponse getBook(@PathVariable(name = "id") Long id) throws NotFoundException {
		BookResponse bookResponse = this.findBookById(id);
		if (bookResponse == null) {
			log.info("Il libro con ID [{}] non è stato trovato", id);
			new NotFoundException();
		}

		log.info("Il libro con id [{}]: è: {}", id, bookResponse);
		return bookResponse;
	}


	@GetMapping(value = "/author/{author}", produces = {MediaType.APPLICATION_JSON_VALUE})

	public List<BookResponse> searchByAuthor(@PathVariable(name = "author") String author) {

		List<BookResponse> authorlist = new ArrayList<>();
		for (BookResponse book : List) {

			if (book.getAuthor().toLowerCase().contains(author.toLowerCase())) {
				authorlist.add(book);

			} else {
				log.warn("L'autore è sbagliato " + author + " not found.");
			}
		}

		return authorlist;
	}

	@PostMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
	public Long createBook(
			@RequestBody BookRequest bookRequest) {
		log.info("Creazione di un nuovo libro [{}]", bookRequest);
		long id = this.lastId();
		log.info("Il nuovo ID: [{}]", id);

		String title = bookRequest.getTitle() != null ? bookRequest.getTitle() : "Titolo del libro";
		String author = bookRequest.getAuthor() != null ? bookRequest.getAuthor() : "Autore del libro";
		String ISDB = bookRequest.getISDB() != null ? bookRequest.getISDB() : "CODICE_ISDB del libro";

		BookResponse bookResponse = new BookResponse (id, title,author,ISDB);
		this.List.add(bookResponse);
		return id;
	}

	@PutMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public void editBook(
			@PathVariable(name = "id") Long id,
			@RequestBody BookRequest bookRequest ) {

		log.info("modifica di un libro [{}]", id, bookRequest);

		this.updateBooksyId(id, bookRequest);

	}

	@DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public void deleteBook(
			@PathVariable(name = "id") Long id
	) {
		log.info("Eliminazione di un libro [{}]", id);
		this.deleteBookById(id);
	}

	private BookResponse findBookById(Long id) {
		for (BookResponse bookResponse : this.List) {
			if (bookResponse.getId().equals(id)) {
				return bookResponse;
			}
		}

		return null;
	}

	private long lastId(){
		Long lastIndex = 1L;
		for ( BookResponse bookResponse : this.List ) {
			if ( bookResponse.getId() >= lastIndex ) {
				lastIndex = bookResponse.getId() +1;
			}
		}

		return lastIndex;
	}
	private void deleteBookById(Long id) {
		// Trova il libro da eliminare utilizzando l'ID specificato
		BookResponse bookResponse = findBookById(id);

		if (bookResponse != null) {
			// Se il libro è stato trovato, rimuovilo dalla lista
			List.remove(bookResponse);
		} else {
			// Se il libro non è stato trovato, gestisci l'errore o registra un messaggio di avviso
			log.warn("Il libro con ID " + id + " not found.");
		}
	}

	private void updateBooksyId(Long id, BookRequest newData) {
		Optional<BookResponse> optionalBook = List.stream()
				.filter(book -> book.getId().equals(id))
				.findFirst();

		if (optionalBook.isPresent()) {
			BookResponse book = optionalBook.get();

			// Aggiorna i campi del libro con i valori forniti nel newData, se disponibili
			book.setTitle(newData.getTitle() != null ? newData.getTitle() : book.getTitle());
			book.setAuthor(newData.getAuthor() != null ? newData.getAuthor() : book.getAuthor());
			book.setISDB(newData.getISDB() != null ? newData.getISDB() : book.getISDB());
		} else {

			// Se nessun libro corrisponde all'ID specificato, registra un messaggio di avviso e restituisci null

			log.warn("No book found with ID: " + id);
		}
	}

}






