package com.ictcampus.lab.base.control.model;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

public class BookRequest {
    private String title;
    private String author;
    private String ISDB;

    public String getTitle() {return title;}
    public void setTitle(final String title) {this.title = title;}

    public String getAuthor() {return author;}
    public void setAuthor(final String author) {this.author = author;}

    public String getISDB() {return ISDB;}
    public void setISDB(final String ISDB) {this.ISDB = ISDB;}
    @Override
    public String toString() {return "BookRequest [title=" + title + ", author=" + author + ", ISDB=";}
}


