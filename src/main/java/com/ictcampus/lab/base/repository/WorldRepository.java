package com.ictcampus.lab.base.repository;

import com.ictcampus.lab.base.repository.entity.WorldEntity;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Types;
import java.util.Arrays;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@AllArgsConstructor
@Repository
public class WorldRepository {
    private JdbcTemplate jdbcTemplate;

    public List<WorldEntity> findAll() {
        return jdbcTemplate.query( "SELECT * FROM world", new BeanPropertyRowMapper<>( WorldEntity.class ) );
    }

    public WorldEntity findById( long id ) {
        return jdbcTemplate.queryForObject( "SELECT * FROM world WHERE id = ?",
                new BeanPropertyRowMapper<>( WorldEntity.class ), id );
    }

    public WorldEntity findByName( String name ) {
        return jdbcTemplate.queryForObject( "SELECT * FROM world WHERE name = ?",
                new BeanPropertyRowMapper<>( WorldEntity.class ), name );
    }

    public List<WorldEntity> findByNameOrSearch( String name, String search ) {
        String filter = name;
        if ( filter == null || filter.isEmpty() ) {
            filter = search;
        }

        filter = "%" + filter + "%";
        return jdbcTemplate.query( "SELECT * FROM world WHERE name ILIKE ? OR system ILIKE ?",
                new BeanPropertyRowMapper<>( WorldEntity.class ), filter, filter );
    }

    @Transactional
    public long create( String name, String system ) {
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        PreparedStatementCreatorFactory preparedStatementCreatorFactory = new PreparedStatementCreatorFactory(
                "INSERT INTO world (name, system) VALUES (?, ?)",
                Types.VARCHAR, Types.VARCHAR
        );
        preparedStatementCreatorFactory.setReturnGeneratedKeys( true );

        PreparedStatementCreator preparedStatementCreator = preparedStatementCreatorFactory
                .newPreparedStatementCreator(
                        Arrays.asList( name, system )
                );


        jdbcTemplate.update( preparedStatementCreator, generatedKeyHolder );

        return generatedKeyHolder.getKey().longValue();
    }

    public int update( long id, String name, String system ) {
        return jdbcTemplate.update( "UPDATE world SET name=?, system=? WHERE id=?", name, system, id );
    }

    public int delete( long id ) {
        return jdbcTemplate.update( "DELETE FROM world WHERE id=?", id );
    }
}
