package com.ictcampus.lab.base.service;

import com.ictcampus.lab.base.control.model.WorldResponse;
import com.ictcampus.lab.base.repository.WorldRepository;
import com.ictcampus.lab.base.repository.entity.WorldEntity;
import com.ictcampus.lab.base.service.model.World;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Service
@AllArgsConstructor
@Slf4j
public class WorldService {
    @Autowired
    private WorldRepository worldRepository;

    public List<World> getWorlds() {
        List<World> worldList = new ArrayList<>();

        List<WorldEntity> worldEntities = worldRepository.findAll();
        for (WorldEntity worldEntity : worldEntities) {
            worldList.add( convertToWorld(worldEntity) );
        }
        return worldList;
    }

    public List<World> getWorlds( String name, String search ) {
        List<World> worldList = new ArrayList<>();

        List<WorldEntity> worldEntities = worldRepository.findByNameOrSearch(name, search);
        for (WorldEntity worldEntity : worldEntities) {
            worldList.add( convertToWorld(worldEntity) );
        }
        return worldList;
    }

    public World getWorldById( Long id ) {
        return convertToWorld( worldRepository.findById( id ) );
    }

    public Long addWorld( String name, String system ) {

        return worldRepository.create( name, system );
    }

    public void editWorld( Long id, String name, String system ) {
        worldRepository.update( id, name, system );
    }

    public void deleteWorld( Long id ) {
        worldRepository.delete( id );
    }

    private World convertToWorld( WorldEntity worldEntity ) {
        World world = new World();
        world.setId( worldEntity.getId() );
        world.setName( worldEntity.getName() );
        world.setSystem( worldEntity.getSystem() );

        return world;
    }
}
