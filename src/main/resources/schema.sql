CREATE SCHEMA IF NOT EXISTS baseproject;
SET SCHEMA baseproject;

CREATE TABLE world (
    id bigint auto_increment,
    name VARCHAR(50) NOT NULL,
    system VARCHAR(255) NULL
);

CREATE UNIQUE INDEX IDX_NAME ON world(name);
CREATE INDEX IDX_SYSTEM ON world(system);